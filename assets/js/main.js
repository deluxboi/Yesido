$( "#inputDate" ).datepicker();

// Couple Countdown
// 
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2018 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="couple-countdown2"
    document.getElementById("couple-countdown2").innerHTML = "<ul class='list-inline'><li class='list-style-none card p-20'><ul><li class='list-style-none f-32 w-600'>" + days + "</li><li class='list-style-none'>Days</li></ul></li> <li class='list-style-none p-20'><ul><li class='list-style-none f-32 w-600'>&colon;</li></ul></li> <li class='list-style-none card p-20'><ul><li class='list-style-none f-32 w-600'>" + hours + "</li><li class='list-style-none'>Hours</li></ul></li> <li class='list-style-none p-20'><ul><li class='list-style-none f-32 w-600'>&colon;</li></ul></li> <li class='list-style-none card p-20'><ul><li class='list-style-none f-32 w-600'>" + minutes + "</li><li class='list-style-none'>Minutes</li></ul></li> <li class='list-style-none p-20'><ul><li class='list-style-none f-32 w-600'>&colon;</li></ul></li> <li class='list-style-none card p-20'><ul><li class='list-style-none f-32 w-600'>" + seconds + "</li><li class='list-style-none'>Seconds</li></ul></li></ul>";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("couple-countdown2").innerHTML = "EXPIRED";
    }
}, 1000);